package mt2.decoder.stack;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import mt2.constants.ConfigKeys;
import mt2.decoder.TranslationHypothesis;
import mt2.util.ComparatorUtils;
import mt2.util.Factory;
import mt2.util.StringUtils;

public abstract class Stack implements Iterable<TranslationHypothesis> {
    protected final Comparator<Map.Entry<Integer, TranslationHypothesis>> STACK_COMPARATOR = ComparatorUtils.newEntrySetComparator(false);
    protected Map<Integer, TranslationHypothesis> stack;

    public Stack() { this.stack = new HashMap<>(); }

    /** Put a hypothesis onto the stack and recombine/prune (if needed) */
    public void push(TranslationHypothesis hypothesis) {
        int hashCode = hypothesis.hashRepresentation();
        if (stack.containsKey(hashCode)) hypothesis.recombineWith(stack.get(hashCode));
        stack.put(hashCode, hypothesis);
        prune();
    }

    public abstract void prune();

    public int size() { return stack.size(); }
    public boolean isEmpty() { return size() == 0; }

    @Override
    public Iterator<TranslationHypothesis> iterator() { return stack.values().iterator(); }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("v----v----v\n");
        for (TranslationHypothesis hypothesis : this) sb.append(hypothesis).append('\n');
        sb.append("^----^----^");
        return sb.toString();
    }

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// Utils //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public static Factory<Stack> createStackFactoryUsingProperties(final Properties config) throws IllegalArgumentException {
        String stackClassName = config.getProperty(ConfigKeys.STACK_TYPE);
        final Class<?> stackClass;
        try {
            stackClass = Class.forName(StringUtils.ensureHasPrefix(stackClassName, Stack.class.getPackage().getName() + "."));
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("unrecognized stack class: " + stackClassName);
        }
        if (stackClass.equals(UnrestrictedStack.class)) {
            return new Factory<Stack>() {
                @Override
                public Stack newInstance() { return new UnrestrictedStack(); }
            };
        }
        if (stackClass.equals(HistogramPruningStack.class)) {
            String maxSizeConfig = config.getProperty(ConfigKeys.STACK_MAX_SIZE);
            if (maxSizeConfig == null) throw new IllegalArgumentException("need to specify " + ConfigKeys.STACK_MAX_SIZE + " configuration option when using stack type " + stackClassName);
            final int maxSize = Integer.parseInt(maxSizeConfig);
            return new Factory<Stack>() {
                @Override
                public Stack newInstance() { return new HistogramPruningStack(maxSize); }
            };
        }
        if (stackClass.equals(ThresholdPruningStack.class)) {
            String thresholdConfig = config.getProperty(ConfigKeys.STACK_THRESHOLD);
            if (thresholdConfig == null) throw new IllegalArgumentException("need to specify " + ConfigKeys.STACK_THRESHOLD + " configuration option when using stack type " + stackClassName);
            final double threshold = Double.parseDouble(thresholdConfig);
            return new Factory<Stack>() {
                @Override
                public Stack newInstance() { return new ThresholdPruningStack(threshold); }
            };
        }
        throw new IllegalStateException();
    }
}