package mt2.decoder.stack;

import java.util.Map;
import java.util.SortedSet;

import mt2.decoder.TranslationHypothesis;
import mt2.util.ComparatorUtils;

public class ThresholdPruningStack extends Stack {
    private final double threshold;

    public ThresholdPruningStack(double threshold) {
        if (threshold <= 0 || threshold > 1) throw new IllegalArgumentException("threshold should be in (0,1]");
        this.threshold = threshold;
    }

    @Override
    public void prune() {
        SortedSet<Map.Entry<Integer, TranslationHypothesis>> sortedEntries = ComparatorUtils.entriesSortedByValues(stack);
        double bestScore = sortedEntries.last().getValue().getScore();
        for (Map.Entry<Integer, TranslationHypothesis> entry : sortedEntries) {
            TranslationHypothesis worst = entry.getValue();
            if (shouldPrune(worst.getScore(), bestScore)) {
                stack.remove(entry.getKey());
            } else {
                break;
            }
        }
    }

    private boolean shouldPrune(double worstScore, double bestScore) {
        double limit = (bestScore > 0) ? bestScore * threshold : bestScore / threshold;
        return worstScore < limit;
    }
}
