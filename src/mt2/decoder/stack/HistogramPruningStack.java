package mt2.decoder.stack;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import mt2.decoder.TranslationHypothesis;

public class HistogramPruningStack extends Stack {
    private final int maxSize;

    public HistogramPruningStack(int maxSize) {
        if (maxSize <= 0) throw new IllegalArgumentException("maxSize should be greater than 0");
        this.maxSize = maxSize;
    }

    @Override
    public void prune() {
        if (size() <= maxSize) return;
        Set<Map.Entry<Integer, TranslationHypothesis>> entries = stack.entrySet();
        Map.Entry<Integer, TranslationHypothesis> worstEntry = Collections.min(entries, STACK_COMPARATOR);
        stack.remove(worstEntry.getKey());
    }
}