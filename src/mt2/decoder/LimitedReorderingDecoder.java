package mt2.decoder;

import mt2.decoder.stack.Stack;
import mt2.phrasetable.PhraseTable;
import mt2.phrasetable.TranslationOption;
import mt2.util.Factory;

public class LimitedReorderingDecoder extends UnlimitedReorderingDecoder {
    private final int neighborhood;

    public LimitedReorderingDecoder(PhraseTable phraseTable, Factory<Stack> stackFactory, int neighborhood) {
        super(phraseTable, stackFactory);
        if (neighborhood < 0) throw new IllegalArgumentException("neighborhood should be greater than zero");
        this.neighborhood = neighborhood;
    }

    @Override
    protected int getTranslationSourcePositionStart(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        return hypothesis.wordCoverEnd() - neighborhood;
    }

    @Override
    protected int getTranslationSourcePositionEnd(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        return hypothesis.wordCoverEnd() + neighborhood;
    }
}