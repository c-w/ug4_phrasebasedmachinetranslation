package mt2.decoder;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import mt2.constants.ConfigKeys;
import mt2.decoder.stack.Stack;
import mt2.phrasetable.PhraseTable;
import mt2.phrasetable.TranslationOption;
import mt2.util.Factory;
import mt2.util.IOUtils;
import mt2.util.StringUtils;


///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// Decoder ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

public abstract class Decoder {
    private final PhraseTable phraseTable;
    private final Factory<Stack> stackFactory;
    private List<Stack> stacks;

    public Decoder(PhraseTable phraseTable, Factory<Stack> stackFactory) {
        this.phraseTable = phraseTable;
        this.stackFactory = stackFactory;
    }

    private void initializeStacks(String sourceSentence) {
        List<String> sourceWords = PhraseTable.tokenize(sourceSentence);
        int numStacks = sourceWords.size() + 1;
        stacks = new ArrayList<>(numStacks);
        for (int i = 0; i < numStacks; i++) stacks.add(stackFactory.newInstance());
        stacks.get(0).push(new TranslationHypothesis(sourceWords));
    }

    /**
     * Stack based decoding algorithm.
     * Based on the pseudo-code in Koehn's book (Figure 6.6, page 165)
     */
    public void decode(String sourceSentence) {
        initializeStacks(sourceSentence);
        for (Stack stack : stacks) {
            for (TranslationHypothesis hypothesis : stack) {
                for (TranslationOption translationOption : phraseTable) {
                    expand(hypothesis, translationOption);
                }
            }
        }
    }

    private void expand(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        List<TranslationHypothesis> multiHypothesis = extend(hypothesis, translationOption);
        for (TranslationHypothesis extendedHypothesis:multiHypothesis) {stacks.get(extendedHypothesis.numWordsCovered()).push(extendedHypothesis);}
    }

    /**
     * Extend an hypothesis with a translation rule.
     * Returns an empty list if the translation rule does not apply to the hypothesis
     */
    protected abstract List<TranslationHypothesis> extend(TranslationHypothesis hypothesis, TranslationOption translationOption);

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stacks.size(); i++) {
            Stack stack = stacks.get(i);
            sb.append("Stack ").append(i).append('=').append(stack.size()).append(' ');
        }
        sb.deleteCharAt(sb.length() - 1).append('\n');
        Stack lastStack = stacks.get(stacks.size() - 1);
        int i = 0;
        for (TranslationHypothesis hypothesis : lastStack) {
            sb.append(i)
                .append(' ').append(hypothesis.translationString())
                .append("  ").append("score=").append(hypothesis.getScore())
                .append('\n');
            i++;
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// Utils //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    private static Decoder createDecoderUsingProperties(PhraseTable phraseTable, Factory<Stack> stackFactory, Properties config) throws IllegalArgumentException {
        String decoderClassName = config.getProperty(ConfigKeys.DECODER_TYPE);
        final Class<?> decoderClass;
        try {
            decoderClass = Class.forName(StringUtils.ensureHasPrefix(decoderClassName, Decoder.class.getPackage().getName() + "."));
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("unrecognized decoder class: " + decoderClassName);
        }
        if (decoderClass.equals(MonotonicDecoder.class)) {
            return new MonotonicDecoder(phraseTable, stackFactory);
        }
        if (decoderClass.equals(UnlimitedReorderingDecoder.class)) {
            return new UnlimitedReorderingDecoder(phraseTable, stackFactory);
        }
        if (decoderClass.equals(LimitedReorderingDecoder.class)) {
            String neighborhoodConfig = config.getProperty(ConfigKeys.DECODER_NEIGHBORHOOD);
            if (neighborhoodConfig == null) throw new IllegalArgumentException("need to specify neighborhood when using " + decoderClassName);
            final int neighborhood = Integer.parseInt(neighborhoodConfig);
            return new LimitedReorderingDecoder(phraseTable, stackFactory, neighborhood);
        }
        throw new IllegalStateException();
    }

    public static void main(String[] args) {
        // parse arguments
        if (args.length < 3) {
            System.err.println(
                "need to specify three arguments:\n" +
                "\tpath to decoder configuration file\n" +
                "\tpath to phrase table to use\n" +
                "\tpath to source sentences to translate\n");
            System.exit(1);
        }
        String configPath = args[0];
        String phraseTablePath = args[1];
        String sourceSentencesPath = args[2];

        // load configuration
        final Properties config = new Properties();
        try {
            config.load(new FileReader(configPath));
        } catch (IOException e) {
            System.err.println("couldn't load configuration file: " + e.getMessage());
            System.exit(1);
        }

        // load decoder
        Decoder decoder = null;
        try {
            decoder = createDecoderUsingProperties(
                PhraseTable.createPhraseTableUsingProperties(phraseTablePath, config),
                Stack.createStackFactoryUsingProperties(config),
                config);
        } catch (IllegalArgumentException e) {
            System.err.println("error parsing configuration file: " + e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println("error loading phrase table: " + e.getMessage());
            System.exit(1);
        }

        // load sentences to translate
        List<String> sourceSentences = null;
        try {
            sourceSentences = IOUtils.fileinput(sourceSentencesPath);
        } catch (IOException e) {
            System.err.println("error loading source sentences: " + e.getMessage());
            System.exit(1);
        }

        // run decoder
        for (String sourceSentence : sourceSentences) {
            decoder.decode(sourceSentence);
            System.out.println(decoder);
        }
    }
}
