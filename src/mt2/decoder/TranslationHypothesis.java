package mt2.decoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mt2.constants.Constants;
import mt2.phrasetable.TranslationOption;
import mt2.util.JSONifiable;
import mt2.util.StringUtils;

public class TranslationHypothesis extends JSONifiable implements Comparable<TranslationHypothesis> {
    private final List<String> sourceWords;
    private final boolean[] wordsCovered;
    private double score;
    private TranslationHypothesis previousTranslationHypothesis;
    private final List<String> translation;

    public TranslationHypothesis(List<String> sourceWords) {
        this.sourceWords = sourceWords;
        this.wordsCovered = new boolean[sourceWords.size()];
        this.score = 0.0;
        this.previousTranslationHypothesis = null;
        this.translation = new ArrayList<>();
    }

    public double getScore() { return score; }
    public int getNumSourceWords() { return sourceWords.size(); }
    public List<String> getSourceWords() { return sourceWords; }
    public List<String> getSourceWords(int start, int stop) { return sourceWords.subList(start, stop); }

    public int wordCoverEnd() {
        int end = -1;
        for (int i = 0; i < wordsCovered.length; i++) if (wordsCovered[i]) end = i;
        return end + 1;
    }

    public int numWordsCovered() {
        int numWordsCovered = 0;
        for (boolean covered : wordsCovered) if (covered) numWordsCovered++;
        return numWordsCovered;
    }

    public boolean noWordCovered(int start, int end){
        for (int i = start; i < end; i++) if (wordsCovered[i]) return false;
        return true;
    }

    public boolean canBeAppliedTo(TranslationOption translationOption, int translationSourcePosition) {
        List<String> translationWords = translationOption.getSource();
        int numWordsToTranslate = translationWords.size();
        if (translationSourcePosition < 0 || translationSourcePosition + numWordsToTranslate > getNumSourceWords()) return false;
        if (!noWordCovered(translationSourcePosition, translationSourcePosition + numWordsToTranslate)) return false;
        List<String> nextHypothesisWords = getSourceWords(translationSourcePosition, translationSourcePosition + numWordsToTranslate);
        if (!translationWords.equals(nextHypothesisWords)) return false;
        return true;
    }

    public boolean applicable(int start, int end){
        for (int i = start; i < end; i++) if (wordsCovered[i]) return false;
        return true;
    }

    public void recombineWith(TranslationHypothesis other) {
        if (other.score > score) {
            score = other.score;
            previousTranslationHypothesis = other.previousTranslationHypothesis;
        } else {
            other.score = score;
            other.previousTranslationHypothesis = previousTranslationHypothesis;
        }
    }

    public TranslationHypothesis extend(TranslationOption translationOption, int translationOptionStart, int translationOptionEnd) {
        TranslationHypothesis extendedHypothesis = copy();
        extendedHypothesis.score = score + translationOption.getScore();
        extendedHypothesis.previousTranslationHypothesis = this;
        extendedHypothesis.translation.addAll(translationOption.getTarget());
        for (int i = translationOptionStart; i < translationOptionEnd; i++) extendedHypothesis.wordsCovered[i] = true;
        return extendedHypothesis;
    }

    public TranslationHypothesis copy() {
        TranslationHypothesis copyHypothesis = new TranslationHypothesis(sourceWords);
        System.arraycopy(wordsCovered, 0, copyHypothesis.wordsCovered, 0, wordsCovered.length);
        copyHypothesis.score = score;
        copyHypothesis.previousTranslationHypothesis = previousTranslationHypothesis;
        copyHypothesis.translation.addAll(translation);
        return copyHypothesis;
    }

    @Override
    public int compareTo(TranslationHypothesis other) { return (int)Math.signum(score - other.score); }

    /**
     * Hash-code should <b>not</b> include score or previous hypothesis.
     * Used for hypothesis re-combination: hash collisions can be recombined
     * This functionality is <b>not</b> implemented by overriding
     * <tt>hashCode</tt> because that would violate the contract that two
     * objects for which <tt>equals</tt> returns <tt>true</tt> should return
     * the same hashCode
     */
    public int hashRepresentation() {
        int hashCode = 17;
        hashCode = 37 * hashCode + sourceWords.hashCode();
        hashCode = 37 * hashCode + Arrays.hashCode(wordsCovered);
        hashCode = 37 * hashCode + translation.hashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (!(other instanceof TranslationHypothesis)) return false;
        TranslationHypothesis hypothesis = (TranslationHypothesis)other;
        return sourceWords.equals(hypothesis.sourceWords)
            && Arrays.equals(wordsCovered, hypothesis.wordsCovered)
            && score == hypothesis.score
            && previousTranslationHypothesis.equals(hypothesis.previousTranslationHypothesis)
            && translation.equals(hypothesis.translation);
    }

    @Override
    public String toJSON(int indent) {
        StringBuilder sb = new StringBuilder();
        sb.append(JSONifiable.makeIndent(indent)).append('{');
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"score\": ").append(score).append(',');
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"translation\": \"").append(translationString()).append("\",");
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"wordsCovered\": \"");
        for (boolean covered : wordsCovered) sb.append(covered ? '*' : '-');
        sb.append("\",");
        if (previousTranslationHypothesis != null) {
            sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
                .append("\"previousHypothesis\": ").append(previousTranslationHypothesis.toJSON(indent + 1));
        } else {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append('\n').append(JSONifiable.makeIndent(indent)).append('}');
        return sb.toString();
    }

    public String translationString() { return StringUtils.join(translation, Constants.WORD_SEP); }

}