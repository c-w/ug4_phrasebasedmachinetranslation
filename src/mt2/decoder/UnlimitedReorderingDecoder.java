package mt2.decoder;

import java.util.ArrayList;
import java.util.List;

import mt2.decoder.stack.Stack;
import mt2.phrasetable.PhraseTable;
import mt2.phrasetable.TranslationOption;
import mt2.util.Factory;

public class UnlimitedReorderingDecoder extends Decoder {
    public UnlimitedReorderingDecoder(PhraseTable phraseTable, Factory<Stack> stackFactory) { super(phraseTable, stackFactory); }

    /** Extend hypotheses with unlimited reordering. */
    @Override
    protected  List<TranslationHypothesis> extend(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        List<TranslationHypothesis> multiHypothesis = new ArrayList<>();
        List<String> translationWords = translationOption.getSource();
        int numWordsToTranslate = translationWords.size();
        // make sure that there is the required number of words left to translate
        int sourcePositionStart = Math.max(0, getTranslationSourcePositionStart(hypothesis, translationOption));
        int sourcePositionEnd = Math.min(hypothesis.getNumSourceWords(), getTranslationSourcePositionEnd(hypothesis, translationOption));
        // find a position at which we can apply the translation option
        for (int translationSourcePosition = sourcePositionStart; translationSourcePosition <= sourcePositionEnd; translationSourcePosition++) {
            if (hypothesis.canBeAppliedTo(translationOption, translationSourcePosition)) {
                TranslationHypothesis extendedHypothesis = hypothesis.extend(translationOption, translationSourcePosition, translationSourcePosition + numWordsToTranslate);
                multiHypothesis.add(extendedHypothesis);
            }
        }
        return multiHypothesis;
    }

    protected int getTranslationSourcePositionStart(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        return 0;
    }

    protected int getTranslationSourcePositionEnd(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        return hypothesis.getNumSourceWords();
    }
}