package mt2.decoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mt2.decoder.stack.Stack;
import mt2.phrasetable.PhraseTable;
import mt2.phrasetable.TranslationOption;
import mt2.util.Factory;

public class MonotonicDecoder extends Decoder {
    public MonotonicDecoder(PhraseTable phraseTable, Factory<Stack> stackFactory) { super(phraseTable, stackFactory); }

    /** Extend hypotheses in a continuous block from left to right. */
    @Override
    protected List<TranslationHypothesis> extend(TranslationHypothesis hypothesis, TranslationOption translationOption) {
        List<TranslationHypothesis> multiHypothesis = new ArrayList<>();
        List<String> translationWords = translationOption.getSource();
        int numWordsToTranslate = translationWords.size();
        int oldWordCoverEnd = hypothesis.wordCoverEnd();
        int newWordCoverEnd = oldWordCoverEnd + numWordsToTranslate;
        // make sure that there is the required number of words left to translate
        if (newWordCoverEnd > hypothesis.getNumSourceWords()) return Collections.<TranslationHypothesis>emptyList();
        // make sure that the translation matches the source words
        List<String> nextHypothesisWords = hypothesis.getSourceWords(oldWordCoverEnd, newWordCoverEnd);
        if (!translationWords.equals(nextHypothesisWords)) return Collections.<TranslationHypothesis>emptyList();
        // generate a new hypothesis by applying the translation
        multiHypothesis.add(hypothesis.extend(translationOption, oldWordCoverEnd, newWordCoverEnd));
        return multiHypothesis;
    }
}