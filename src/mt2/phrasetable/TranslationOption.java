package mt2.phrasetable;

import java.util.List;

import mt2.constants.Constants;
import mt2.util.JSONifiable;
import mt2.util.StringUtils;

public class TranslationOption extends JSONifiable {
    private final List<String> source;
    private final List<String> target;
    private final Double score;
    private final String sourceString;
    private final String targetString;

    public TranslationOption(List<String> source, List<String> target, Double score) {
        this.source = source;
        this.target = target;
        this.score = score;
        this.sourceString = StringUtils.join(source, Constants.WORD_SEP);
        this.targetString = StringUtils.join(target, Constants.WORD_SEP);
    }

    public List<String> getSource() { return source; }
    public List<String> getTarget() { return target; }
    public Double getScore() { return score; }
    public String getSourceAsString() { return sourceString; }
    public String getTargetAsString() { return targetString; }

    @Override
    public int hashCode() {
        int s = getSource() == null ? 0 : getSource().hashCode();
        int t = getTarget() == null ? 0 : getTarget().hashCode();
        int p = getScore() == null ? 0 : getScore().hashCode();
        return s ^ t ^ p;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (!(other instanceof TranslationOption)) return false;
        TranslationOption phraseTableEntry = (TranslationOption)other;
        return phraseTableEntry.getSource().equals(getSource())
            && phraseTableEntry.getTarget().equals(getTarget())
            && phraseTableEntry.getScore().equals(getScore());
    }

    @Override
    public String toJSON(int indent) {
        StringBuilder sb = new StringBuilder();
        sb.append(JSONifiable.makeIndent(indent)).append('{');
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"source\": \"").append(getSourceAsString()).append("\",");
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"target\": \"").append(getTargetAsString()).append("\",");
        sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
            .append("\"score\": ").append(getScore());
        sb.append('\n').append(JSONifiable.makeIndent(indent)).append('}');
        return sb.toString();
    }
}