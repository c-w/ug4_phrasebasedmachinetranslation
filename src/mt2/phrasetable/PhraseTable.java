package mt2.phrasetable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.regex.Pattern;

import mt2.constants.ConfigKeys;
import mt2.constants.Constants;
import mt2.util.IOUtils;
import mt2.util.JSONifiable;
import mt2.util.ComparatorUtils;


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// PhraseTable /////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

public class PhraseTable implements Iterable<TranslationOption> {
    private final Map<List<String>, List<TranslationOption>> map;

    /**
     * Constructs a PhraseTable by parsing a file.
     * Every line in the file should be formatted as per the specification in
     * the assignment handout:
     * <tt>source-word ||| target-word ||| translation-model-score ||| |||</tt>
     */
    public static PhraseTable readFromFile(String path) throws IOException {
        PhraseTable phraseTable = new PhraseTable();
        BufferedReader br = new BufferedReader(new FileReader(new File(path)));
        try {
            String line = br.readLine();
            String delim = Pattern.quote(Constants.FIELD_SEP);
            while (line != null) {
                String[] fields = line.split(delim);
                List<String> source = tokenize(fields[0].trim());
                List<String> target = tokenize(fields[1].trim());
                Double score = Double.parseDouble(fields[2]);
                phraseTable.put(source, target, score);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return phraseTable;
    }

    private PhraseTable() { this.map = new LinkedHashMap<>(); }

    public void limitTranslationOptions(int maxTranslationOptions) {
        if (maxTranslationOptions <= 0) throw new IllegalArgumentException("maxTranslationOptions should be greater than 0");
        for (Map.Entry<List<String>, List<TranslationOption>> entry : map.entrySet()) {
            List<TranslationOption> translationOptions = entry.getValue();
            Collections.sort(translationOptions, new Comparator<TranslationOption>() {
                @Override
                public int compare(TranslationOption a, TranslationOption b) {
                    return -a.getScore().compareTo(b.getScore());
                }
            });
            while (translationOptions.size() > maxTranslationOptions) translationOptions.remove(translationOptions.size() - 1);
        }
    }

    @Override
    public Iterator<TranslationOption> iterator() {
        return new Iterator<TranslationOption>() {
            private final Iterator<Iterator<TranslationOption>> iteratorIterator = getIterators();
            private Iterator<TranslationOption> currentIterator = iteratorIterator.next();

            private Iterator<Iterator<TranslationOption>> getIterators() {
                List<Iterator<TranslationOption>> iterators = new ArrayList<>();
                for (List<TranslationOption> entries : map.values()) iterators.add(entries.iterator());
                return iterators.iterator();
            }

            private void updateCurrentIterator() { if (!currentIterator.hasNext()) currentIterator = iteratorIterator.next(); }

            @Override
            public boolean hasNext() {
                try {
                    updateCurrentIterator();
                } catch (NoSuchElementException e) {
                    return false;
                }
                return currentIterator.hasNext();
            }

            @Override
            public void remove() { currentIterator.remove(); }

            @Override
            public TranslationOption next() {
                updateCurrentIterator();
                return currentIterator.next();
            }
        };
    }

    /** Record a candidate translation for a source string. */
    private void put(List<String> source, List<String> target, Double score) {
        if (!map.containsKey(source)) {
            map.put(source, new ArrayList<TranslationOption>());
        }
        map.get(source).add(new TranslationOption(source, target, score));
    }

    private void validate() {
        // make sure that the translation probabilities sum up to one
        for (Map.Entry<List<String>, List<TranslationOption>> entry : map.entrySet()) {
            List<String> source = entry.getKey();
            double score = 0;
            for (TranslationOption phraseTableEntry : entry.getValue()) {
                score += phraseTableEntry.getScore();
            }
            if (ComparatorUtils.doubleEquals(score, 1.0)) {
                throw new IllegalStateException(
                    "entries for source words '" + source + "' " +
                    "do not add up to 1.0 (actual value: " + score + ")");
            }
        }
    }

    public List<Span> getSpans(String sourceSentence, boolean sparsify) {
        List<Span> spans = new ArrayList<>();
        List<String> sourceWords = tokenize(sourceSentence);
        for (int i = 0; i < sourceWords.size(); i++) {
            for (int j = i + 1; j < sourceWords.size() + 1; j++) {
                List<String> span = sourceWords.subList(i, j);
                List<TranslationOption> entries = map.containsKey(span)
                    ? map.get(span)
                    : Collections.<TranslationOption>emptyList();
                if (!sparsify || entries.size() != 0) {
                    spans.add(new Span(i, j + 1, entries));
                }
            }
        }
        return spans;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (TranslationOption phraseTableEntry : this) {
            sb.append(phraseTableEntry.getSourceAsString())
                .append(' ').append(Constants.FIELD_SEP).append(' ')
                .append(phraseTableEntry.getTargetAsString())
                .append(' ').append(Constants.FIELD_SEP).append(' ')
                .append(phraseTableEntry.getScore())
                .append(' ').append(Constants.FIELD_SEP).append(' ')
                .append(Constants.FIELD_SEP).append('\n');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// Span //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    private static class Span extends JSONifiable {
        private final int start;
        private final int stop;
        private final List<TranslationOption> entries;

        public Span(int start, int stop, List<TranslationOption> entries) {
            this.start = start;
            this.stop = stop;
            this.entries = entries;
        }

        public int getStart() { return start; }
        public int getStop() { return stop; }
        public List<TranslationOption> getEntries() { return entries; }

        @Override
        public int hashCode() {
            int entries = getEntries() == null ? 0 : getEntries().hashCode();
            return getStart() ^ getStop() ^ entries;
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) return false;
            if (!(other instanceof Span)) return false;
            Span span = (Span)other;
            return span.getStart() == getStart()
                && span.getStop() == getStop()
                && span.getEntries().equals(getEntries());
        }

        @Override
        public String toJSON(int indent) {
            StringBuilder sb = new StringBuilder();
            sb.append(JSONifiable.makeIndent(indent)).append('{');
            sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
                .append("\"start\": ").append(getStart()).append(',');
            sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
                .append("\"stop\": ").append(getStop()).append(',');
            sb.append('\n').append(JSONifiable.makeIndent(indent + 1))
                .append("\"entries\": [");
            for (TranslationOption phraseTableEntry : getEntries()) {
                sb.append('\n')
                    .append(phraseTableEntry.toJSON(indent + 2)).append(',');
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append('\n').append(JSONifiable.makeIndent(indent + 1)).append(']');
            sb.append('\n').append(JSONifiable.makeIndent(indent)).append('}');
            return sb.toString();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// Utils //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public static PhraseTable createPhraseTableUsingProperties(String phraseTablePath, Properties config) throws IOException, IllegalArgumentException {
        PhraseTable phraseTable = PhraseTable.readFromFile(phraseTablePath);
        String maxTranslationOptionsConfig = config.getProperty(ConfigKeys.PHRASE_TABLE_MAX_TRANSLATION_OPTIONS);
        if (maxTranslationOptionsConfig != null) phraseTable.limitTranslationOptions(Integer.parseInt(maxTranslationOptionsConfig));
        return phraseTable;
    }

    /** Split a sentence into words. */
    public static List<String> tokenize(String sentence) {
        return Arrays.asList(sentence.split(Pattern.quote(Constants.WORD_SEP)));
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println(
                "need to specify two arguments " +
                "(path to phrase table and sentence to parse)");
            System.exit(1);
        }
        PhraseTable phraseTable = PhraseTable.readFromFile(args[0]);
        List<String> sourceSentences = IOUtils.fileinput(args[1]);

        for (String sourceSentence : sourceSentences) {
            for (Span span : phraseTable.getSpans(sourceSentence, false)) {
                int start = span.getStart();
                int stop = span.getStop();
                List<TranslationOption> entries = span.getEntries();
                if (entries.size() == 0) {
                    System.out.println(start + ".." + stop + " nothing found");
                } else {
                    System.out.println(start + ".." + stop + " found " + entries.size());
                    for (TranslationOption phraseTableEntry : entries) {
                        String target = phraseTableEntry.getTargetAsString();
                        Double score = phraseTableEntry.getScore();
                        System.out.println(target + " " + Constants.FIELD_SEP + " " + score);
                    }
                }
            }
        }
    }
}
