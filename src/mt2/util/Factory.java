package mt2.util;

public interface Factory<T> {
    public T newInstance();
}
