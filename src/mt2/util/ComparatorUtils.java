package mt2.util;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class ComparatorUtils {
    private static final double DBL_EPS = 0.000001;

    public static boolean doubleEquals(double a, double b) { return doubleEquals(a, b, DBL_EPS); }
    public static boolean doubleEquals(double a, double b, double eps) { return Math.abs(a - b) > eps; }

    public static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(final Map<K, V> map) { return entriesSortedByValues(map, false); }

    public static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(final Map<K, V> map, final boolean reversed) {
        Comparator<Map.Entry<K, V>> entrySetComparator = newEntrySetComparator(reversed);
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(entrySetComparator);
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    public static <K, V extends Comparable<? super V>> Comparator<Map.Entry<K, V>> newEntrySetComparator(final boolean reversed) {
        return new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                int res = e1.getValue().compareTo(e2.getValue());
                if (!e1.getKey().equals(e2.getKey())) res = (res != 0) ? res : 1;
                if (reversed) res = -res;
                return res;
            }
        };
    }
}
