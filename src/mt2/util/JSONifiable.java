package mt2.util;

/**
 * Top level class for objects that have a JSON representation.
 * A lot of objects in this assignment are basically glorified key-value
 * stores... representing the objects using JSON makes it easy to visualize this
 * type of objects.
 */
public abstract class JSONifiable {
    private static final String INDENT = "  ";

    @Override
    public String toString() {
        return toJSON(0);
    }

    public abstract String toJSON(int indent);

    public static String makeIndent(int indent) {
        if (indent <= 0) return "";
        StringBuilder sb = new StringBuilder(1 + indent * INDENT.length());
        for (int i = 0; i < indent; i++) sb.append(INDENT);
        return sb.toString();
    }
}
