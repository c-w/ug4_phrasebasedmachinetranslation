package mt2.util;

import java.util.List;

public class StringUtils {

    public static String join(List<String> list, String delimiter) {
        if (list == null || delimiter == null) return null;
        int joinedSize = 0;
        for (String string : list) joinedSize += (string.length() + delimiter.length());
        StringBuilder sb = new StringBuilder(joinedSize);
        for (String string : list) sb.append(string).append(delimiter);
        if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static String ensureHasPrefix(String string, String prefix) {
        if (!string.startsWith(prefix)) return prefix + string;
        return string;
    }

}
