package mt2.constants;

public class ConfigKeys {
    public static final String DECODER_TYPE = "decoder.class";
    public static final String DECODER_NEIGHBORHOOD = "decoder.neighborhood";
    public static final String STACK_TYPE = "decoder.stack.class";
    public static final String STACK_THRESHOLD = "decoder.stack.threshold";
    public static final String STACK_MAX_SIZE = "decoder.stack.maxSize";
    public static final String PHRASE_TABLE_MAX_TRANSLATION_OPTIONS = "phraseTable.maxTranslationOptions";
}
