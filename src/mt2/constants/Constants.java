package mt2.constants;

public class Constants {
    /** Delimiter used to split sentences into individual words. */
    public static final String WORD_SEP = " ";

    /** Delimiter separating the fields of an on-disk phrase table. */
    public static final String FIELD_SEP = "|||";
}
