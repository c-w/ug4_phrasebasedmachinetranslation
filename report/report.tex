\documentclass[a4paper,twocolumn,oneside]{article}

\usepackage[margin=0.25in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{parskip}
\usepackage{titling}
\usepackage{amssymb}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{graphicx}

\titlespacing\section{0pt}{0pt plus 0pt minus 0pt}{0pt plus 0pt minus 0pt}
\titlespacing\subsection{0pt}{0pt plus 0pt minus 0pt}{0pt plus 0pt minus 0pt}
\setlength{\droptitle}{-4em}
\date{\vspace{0em}}
\pagenumbering{gobble}

\title{Machine Translation --- Assignment 2}
\author{Clemens Wolff (s0942284), Jakov \u{S}melkin (s1038803), Toms Bergmanis (s1044253)}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report investigates the stack-based decoding algorithm for phrase-based
statistical machine translation first introduced by \cite{koehn2004}.
Section~\ref{sec:implementation} discusses a toy implementation of the algorithm
using the Java programming language and proposes various schemes to make the
implementation more efficient.\footnote{The implementation can be run using the
following commands.  Parse a phrase-table stored at the path {\tt PT} and find
all the applicable spans in the source sentences stored at the path {\tt SS}:
{\tt java -cp mt2.jar mt2.phrasetable.PhraseTable PT SS}.  Translate the source
sentences stored at the path {\tt SS} using the phrase-table stored at the path
{\tt PT} and the decoding options in the configuration file at {\tt CFG}: {\tt
java -cp mt2.jar mt2.decoder.Decoder CFG PT SS}.  The configuration file grammar
can be found in Table~\ref{tbl:config-language}.} Section~\ref{sec:experiments}
evaluates the performance of the algorithm using toy and real-world inputs.
Section~\ref{sec:conclusion} synthesises the findings and concludes the report.

\section{Implementation}\label{sec:implementation}

The implementation of the stack-based decoding algorithm closely follows the
pseudo-code of Koehn in \cite[Fig.~6.6]{koehn2009}.  Stack-based decoding is a
two-component process.  A decoder uses a phrase table (a definition of how a
source language maps onto a target language) to translate progressively larger
source-spans into target-spans (a span is a sequence of one or more words).  The
two components are discussed in detail below.

\subsection{Phrase table}

A phrase table defines how a span may be translated from one language into
another and how likely that translation is.  A phrase table thus defines a
mapping
$P : \mathfrak{S} \rightarrow \mathfrak{T} \times \mathbb{R}$
where $\mathfrak{S}$ is the space of all spans in the source language and
$\mathfrak{T}$ is the space of all spans in the target language.  Two main
points are of interest when implementing $P$: the space and time efficiency of
the data-structure used to represent the mapping and any restrictions placed on
its codomain.

The phrase table implementation proposed in this report uses a hash table to
store $P$.  Consequentially, elements from $P$ can be accessed in $O(1)$ time
and $O(\mathfrak{S}\mathfrak{T})$ space is required to store $P$.  Note that
space usage is exponential in the size of the source and target language
vocabulary.  Using a hash table to store $P$ will thus not scale and should only
be considered for phrase tables that are trivial in size such as the phrase
table considered in this report ($<100,000$ entries).  In order to implement
bigger phrase tables, the $P$ could be loaded on-demand from disk
\cite{och2002}, space-efficient data-structures could be employed (e.g.\ tries
\cite{och2002}, suffix arrays \cite{callisonburch2005}) or $P$ table could be
compressed (c.f.\ packed tries \cite{germann2009}, phrasal rank encoding
\cite{junczysdowmunt2012}).

This report also implements phrase table pruning and investigates its effects in
Section~\ref{sec:phrase-table-pruning}.  The here-proposed implementation of
phrase table pruning restricts to codomain of every element $s \in
\mathfrak{S}$ to $(\mathfrak{T} \times \mathbb{R})_{n}$: the subset of the $n$
elements $(t, p) \in \mathfrak{T} \times \mathbb{R}$ with the highest values of
$p$.  While this pruning approach is rather crude, similar work from
\cite{johnson2007} suggests that it improves phrase table performance with
little to no loss in decoding quality.  More sophisticated pruning approaches
could involve pruning pairs $(t, p)$ where $p$ is lower than some threshold (as
implemented in \cite{moses}), adapting the cutoff value $n$ depending on the
complexity of $s$ \cite{tomeh2009}, pruning entries based on how often they are
used during the decoding process \cite{eck2007} or using an entropy measure to
discard low-information entries \cite{wang2012}.

\subsection{Stack-based decoder}

The stack-based decoder component of the phrase-based machine translation system
incrementally builds up a translation of a source sentence by expanding
candidate translations using phrase table entries.  The decoder starts with an
empty translation and applies all appropriate translation rules to it.  The
thus-expanded translation-hypotheses get grouped into ``stacks'' of candidates
with the same number of source-sentence words covered and expanded--stacked
recursively (in essence, this is simple beam search).

There are two main levers that affect the performance of the decoder: the number
of hypotheses that are kept for future expansion in any given stack and the way
in which hypotheses are expanded with phrase table entries.

The decoder proposed by this report implements two schemes to limit the number
of hypotheses that are considered at any point in time: histogram pruning and
threshold pruning.  Histogram pruning simply limits the size of every hypothesis
stack to a maximum size $n$.  Whenever a new hypothesis in excess of the
$n$\textsuperscript{th} is added to a stack, the most improbable hypothesis gets
discarded.  Threshold pruning continuously monitors the hypotheses stacks and
discards any hypothesis that is more improbable than $t$ times the likelihood of
the most probable hypothesis in the stack.  The two techniques are compared and
contrasted in Section~\ref{sec:stack-pruning}.

Furthermore, the decoder proposed by this report implements three ways to
control the way in which hypotheses and phrase table entries are combined:
monotonically, with limited re-ordering and with unlimited re-ordering.  The
first strategy only allows hypotheses to be built in one continuous block from
left to right.  Limited re-ordering allows for words within a $k$-token
neighbourhood of the current hypothesis end to be considered next for expansion.
Unlimited re-ordering is a special case of limited re-ordering with $k$ set to
the length of the source sentence.  The effects of the hypothesis re-ordering
strategies on decoder performance are highlighted in
Section~\ref{sec:reordering-strategy}.

The decoding component could be improved in a number of ways.  A distortion
model \cite{quirk2007} or language model \cite{wuebker2012} could be used to
discard hypotheses that are unlikely to lead to good translations.  Search
algorithms other than beam search could be used (e.g.\ a-star \cite{och2001},
dynamic programming \cite{tillman2006}).  Exact decoding algorithms (as opposed
to the approximate algorithms presented so far in this section) could be used
(e.g.\ lagrangian relaxation \cite{chang2011} or any algorithm that works for
the travelling salesman problem \cite{zaslavskiy2009}).

\section{Results}\label{sec:results}

The decoder is used to translate a single sentence from German to English:
\emph{Das ist ein kleines Haus}.  A toy phrase table (9 entries) and a more
realistically sized phrase table (93,586 entries) are used for the task.

Listings~\ref{lst:toy-monotonic-output}--\ref{lst:toy-unlimited-output} show the
final hypotheses resulting from translating the source sentence using the toy
phrase-table (monotonic and unlimited re-ordering strategies).

As we can see, even with such a small phrase table, the size of the stacks
increased sixfold as we go from non-re-ordering version to unlimited re-ordering
version of the decoder.

Decoding with both monotonic and unlimited re-ordering strategies assigns the
same scores to the same translation hypotheses.\footnote{Modulo floating point
rounding errors.}  Furthermore, the fact that the decoder does not use a
language model means that translations that use the same phrases but arrange the
phrases in a different order (e.g.\ \emph{This is an small house} has the same
score as \emph{An small house this is}).  However, this does not mean that using
a non-monotonic decoding strategy is a waste of resources: if our example source
sentence expressed its semantics using a different word order\footnote{Some
languages such as German are very free in word order --- many permutations of
the words in a sentence are still grammatical and carry the same semantics.},
the monotonic decoder might not have found the correctly translated target
sentence.

\section{Experiments}\label{sec:experiments}

Running the decoding algorithm without any optimisations such as stack pruning
or phrase-table pruning is possible using the toy phrase-table but infeasible
using the larger phrase-table: the hypothesis search space is just too large for
the decoding to complete in reasonable time.\footnote{Brief back of the envelope
calculation.  There are 5 words in the source sentence.  There are just under
7,200 translation options per entry in the bigger phrase-table.  A naive
decoding approach using a monotonic re-ordering strategy would therefore need to
examine about $7,200^{5}$ translation hypotheses and even more for unlimited
re-ordering!}\footnote{All run-times are measured using Java's
{\tt System.currentTimeMillis()} on the student.compute DICE machine.  Only
decoding time is counted (phrase-table loading is excluded).}

This section is concerned with finding ways to optimise the decoding process so
that the source sentence can also be translated using the bigger phrase-table.
The example data-set has a convex solution.\footnote{This statement can be
verified by the observation that all of the experiments in
Section~\ref{sec:stack-pruning}--\ref{sec:phrase-table-pruning} selected the
best translation as the most probable output.}  It is therefore possible to
achieve optimal decoder performance (as measured by decoding speed and
translation quality) by tuning the various meta-parameters introduced in
Section~\ref{sec:implementation} individually and combining the independently
obtained optimal parameter values to achieve an overall optimal decoding
solution.  The following sections show the effect that the implementation
choices presented in Section~\ref{sec:implementation} (choice of stack pruning
strategy, choice of re-ordering strategy, choice of phrase-table pruning
strategy) have on decoder performance.

\subsection{Stack pruning}\label{sec:stack-pruning}

The goal of stack pruning is to throw away unlikely translations while we
translate the source sentence.  As we can see in
Tables~\ref{tbl:hist-table}--\ref{tbl:thresh-table}, the run-time of the decoder
grows exponentially as the histogram pruning stack size grows or threshold
pruning's threshold decreases.  Using histogram pruning means that the number of
translation hypotheses (and consequentially decoding run-time) is predictable.
There is a certain upper bound on how long a decoder using histogram pruning run
will run.  This observation does not hold when using threshold pruning.  The
number of translation hypotheses can vastly change over the course of decoding a
sentence.  The same pruning-threshold applied to different sentences can produce
vastly different results (depending on translation scores in the phrase table).

Generally speaking, even with very tight thresholds, the run-time of a decoder
using threshold pruning is worse than the run-time of the same decoder using a
histogram pruning approach.  Additionally, using a histogram pruning approach
gives us a guaranteed upper-bound on the run-time of the decoder.  On the other
hand, only a single translation rule with lots of (seemingly probable)
translation options is required for threshold pruning's stacks to explode and
the decoder to slow to a crawl.  This report therefore argues that histogram
pruning is superior to stack pruning.

\subsection{Re-ordering strategy}\label{sec:reordering-strategy}

As mentioned in Section~\ref{sec:implementation}, there are three re-ordering
strategies: Monotonic (left to right strictly incremental translation), limited
re-ordering and unlimited re-ordering.  Monotonic and unlimited re-ordering are
just the edge cases of limited re-ordering (considering a zero and infinite
re-ordering neighbourhood respectively).  Unlike the heuristics explored in
Section~\ref{sec:stack-pruning}, allowing phrase re-ordering during the
translation process actually adds extra entries to the stack and therefore
increase translation time.  We therefore want to find a balance between
obtaining better translations versus increasing run-time.

The relationship between the size of the re-ordering neighbourhood and run-time
is quite clear.  Monotonic decoding takes the least amount of time to translate
a sentence, followed by limited neighbourhood re-ordering and unlimited
neighbourhood decoding as slowest.  Table~\ref{tbl:comparison} shows that if we
employ heavy stack pruning (or if the source sentence is very short), then there
is not a great difference between the three re-ordering strategies.  However, as
the number of in-flight hypotheses increase, the difference in run-time is
significant: unlimited neighbourhood decoding takes twice as much time to run
than monotonic decoding when more than 32 hypotheses are considered at any given
point in time.

\subsection{Phrase table pruning}\label{sec:phrase-table-pruning}

Lastly, we limit the number of translation options that each entry in the phrase
table may have (i.e.\ every source phrase may only translate to limited number
of phrases in the target language).  This optimisation reduces the number of
possible translation options before decoding even begins.  As displayed in
Table~\ref{tbl:pt-prune}, phrase table pruning vastly decrease the run-time of
the decoder.  However, as with the other optimisations proposed in this section,
we need to balance time efficiency with translation quality (want to leave as
many translations as possible, up to a point where it is not worth the extra
time taken by the decoder).  Figure~\ref{img:unlimited} shows that an optimal
value for this phrase table is 16 translation options (for all tested stack
sizes and re-ordering strategies).

\section{Conclusions}\label{sec:conclusion}

Within the scope of this work we have implemented a stack-based decoding
algorithm for statistical machine translation.  We noted it's limitations with
respect to the computational complexity and investigated the effect of several
heuristics on the reduction of the algorithm's running time.  We have
implemented three different decoding strategies and compared their outputs for
the given input sentence.  We have concluded that with the input sentence given,
there is no benefit from deviating from the monotonic decoding strategy: no
reordering is needed.  We also have concluded that in more realistic scenarios
reordering would be crucial for achieving acceptable translation quality.  The
best performing implementation which gave a correct translation for the given
input sentence was a monotonic decoder with histogram pruning limit 2 and
translation option limit 2 (run-time 40 milliseconds).

\onecolumn
% figures go here

\begin{table}[htbp]
\begin{flushleft}
\begin{tabular}{|l|p{2cm}|r|l|}
\hline
Stack & Translation option limit & \multicolumn{1}{l|}{Time (ms)} & output \\ \hline
\hline
maxSize=2 & None & 1669 & Stack 0=1 Stack 1=2 Stack 2=2 Stack 3=2 Stack 4=2 Stack 5=2 \\ \hline
maxSize=4 & None & 1919 & Stack 0=1 Stack 1=4 Stack 2=4 Stack 3=4 Stack 4=4 Stack 5=4 \\ \hline
maxSize=8 & None & 2664 & Stack 0=1 Stack 1=8 Stack 2=8 Stack 3=8 Stack 4=8 Stack 5=8 \\ \hline
maxSize=16 & None & 4833 & Stack 0=1 Stack 1=16 Stack 2=16 Stack 3=16 Stack 4=16 Stack 5=16 \\ \hline
maxSize=32 & None & 8596 & Stack 0=1 Stack 1=32 Stack 2=32 Stack 3=32 Stack 4=32 Stack 5=32 \\ \hline
maxSize=64 & None & 13651 & Stack 0=1 Stack 1=64 Stack 2=64 Stack 3=64 Stack 4=64 Stack 5=64 \\ \hline
maxSize=128 & None & 38374 & Stack 0=1 Stack 1=128 Stack 2=128 Stack 3=128 Stack 4=128 Stack 5=128 \\ \hline
maxSize=256 & None & 113175 & Stack 0=1 Stack 1=256 Stack 2=256 Stack 3=256 Stack 4=256 Stack 5=256 \\ \hline
maxSize=512 & None & 397078 & Stack 0=1 Stack 1=512 Stack 2=512 Stack 3=512 Stack 4=512 Stack 5=512 \\ \hline
\end{tabular}
\end{flushleft}
\caption{Monotonic decoder histogram pruning run-time experiments}
\label{tbl:hist-table}
\end{table}

\begin{table}[htbp]
\begin{flushleft}
\begin{tabular}{|l|p{2cm}|r|l|}
\hline
Threshold & Translation option limit & \multicolumn{1}{l|}{Time (ms)} & output \\ \hline
\hline
0.5 & 5 & 324 & Stack 0=1 Stack 1=5 Stack 2=25 Stack 3=6 Stack 4=61 Stack 5=305 \\ \hline
0.25 & 5 & 4760 & Stack 0=1 Stack 1=5 Stack 2=30 Stack 3=111 Stack 4=733 Stack 5=3805 \\ \hline
0.125 & 5 & 11855 & Stack 0=1 Stack 1=5 Stack 2=30 Stack 3=180 Stack 4=1079 Stack 5=5814 \\ \hline
0.0625 & 5 & 13032 & Stack 0=1 Stack 1=5 Stack 2=30 Stack 3=180 Stack 4=1079 Stack 5=5814 \\ \hline
\end{tabular}
\end{flushleft}
\caption{Monotonic decoder threshold pruning run-time experiments}
\label{tbl:thresh-table}
\end{table}

\begin{table}[htbp]
\begin{flushleft}
\begin{tabular}{|l|l|p{2cm}|r|}
\hline
Decoder & Stack & \multicolumn{1}{l|}{Translation option limit} & \multicolumn{1}{l|}{Time (ms)} \\ \hline
\hline
MonotonicDecoder & HistogramPruningStack with maxSize=2 & 16 & 70 \\ \hline
UnlimitedDecoder & HistogramPruningStack with maxSize=2 & 16 & 60 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=32 & 16 & 191 \\ \hline
UnlimitedDecoder & HistogramPruningStack with maxSize=32 & 16 & 261 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 16 & 989 \\ \hline
UnlimitedDecoder & HistogramPruningStack with maxSize=512 & 16 & 1687 \\ \hline
\end{tabular}
\end{flushleft}
\caption{Comparison of monotonic and unlimited re-ordering decoder on different stack sizes}
\label{tbl:comparison}
\end{table}

\begin{table}[htbp]
\begin{flushleft}
\begin{tabular}{|l|l|p{2cm}|r|}
\hline
Decoder & Stack & Translation option limit & \multicolumn{1}{l|}{Time} \\ \hline
\hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 2 & 53 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 4 & 352 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 8 & 816 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 16 & 989 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 32 & 1622 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 64 & 3068 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 128 & 5055 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 256 & 8378 \\ \hline
MonotonicDecoder & HistogramPruningStack with maxSize=512 & 512 & 20109 \\ \hline
\end{tabular}
\end{flushleft}
\caption{Phrase table pruning experiments}
\label{tbl:pt-prune}
\end{table}
\begin{figure}[h!]
  \centering
      \includegraphics[scale=0.8]{unlimited.png}
  \caption{Run-time for different translation options with different stack sizes}
  \label{img:unlimited}
\end{figure}
\clearpage

\begin{lstlisting}[label={lst:toy-monotonic-output},caption={Output for decoding with no re-ordering using the toy phrase table}]
Stack 0=1 Stack 1=1 Stack 2=2 Stack 3=4 Stack 4=8 Stack 5=8
0 it is a little house  score=1.9
1 this is a little house  score=2.5
2 it is an small house  score=2.5
3 it is a small house  score=2.1
4 this is an small house  score=3.1
5 this is a small house  score=2.7
6 this is an little house  score=2.9
7 it is an little house  score=2.3
\end{lstlisting}

\begin{lstlisting}[label={lst:toy-unlimited-output},caption={Output for decoding with unlimited re-ordering using the toy phrase table}]
Stack 0=1 Stack 1=6 Stack 2=28 Stack 3=92 Stack 4=192 Stack 5=192
0 it is house a small  score=2.1
1 house this is a little  score=2.5
2 small a house it is  score=2.1
3 house a small this is  score=2.7
4 small house an it is  score=2.5
5 an little this is house  score=2.9000000000000004
6 small house it is an  score=2.5
7 it is a house small  score=2.1
8 small house this is an  score=3.1000000000000005
9 an it is little house  score=2.3
10 small it is a house  score=2.1
11 small a this is house  score=2.7
12 house it is little a  score=1.9000000000000001
13 it is small house a  score=2.1
14 an little it is house  score=2.3
15 little a house it is  score=1.9
16 house little it is an  score=2.3
17 it is little a house  score=1.9000000000000001
18 house small this is a  score=2.7
19 small an it is house  score=2.5
20 it is an house little  score=2.3
21 this is a house small  score=2.7
22 a this is house small  score=2.7
23 a little house this is  score=2.5
24 house little it is a  score=1.9
25 house an this is little  score=2.9
26 this is an small house  score=3.1
27 house an small this is  score=3.0999999999999996
28 little an house it is  score=2.3000000000000003
29 small this is a house  score=2.7
30 house a little it is  score=1.9000000000000001
31 little it is house an  score=2.3
32 an this is house small  score=3.1
33 it is house an small  score=2.5
34 this is a small house  score=2.7
35 a small it is house  score=2.0999999999999996
36 it is small an house  score=2.5
37 little house this is a  score=2.5
38 it is little an house  score=2.3
39 house an it is little  score=2.3
40 an it is house small  score=2.5
41 an small house it is  score=2.5
42 it is a little house  score=1.9
43 house an small it is  score=2.5
44 house small this is an  score=3.1000000000000005
45 this is little house an  score=2.9000000000000004
46 this is house a little  score=2.5
47 little it is an house  score=2.3
48 a little house it is  score=1.9
49 a it is house small  score=2.1
50 a house this is small  score=2.7
51 this is small a house  score=2.7
52 a little it is house  score=1.9
53 a small this is house  score=2.7
54 small an house it is  score=2.5
55 it is an small house  score=2.5
56 this is house a small  score=2.7
57 a it is little house  score=1.9
58 little house a this is  score=2.5
59 small this is house an  score=3.0999999999999996
60 little a house this is  score=2.5
61 house small a this is  score=2.7
62 house it is an small  score=2.5
63 it is an house small  score=2.5
64 a it is small house  score=2.1
65 small it is an house  score=2.5
66 house a this is little  score=2.5
67 a house it is small  score=2.1
68 little an this is house  score=2.9000000000000004
69 house this is an small  score=3.1
70 house little this is a  score=2.5
71 this is house small an  score=3.0999999999999996
72 house a it is little  score=1.9
73 house an little it is  score=2.3000000000000003
74 this is little a house  score=2.5
75 a house little it is  score=1.9000000000000001
76 small it is house an  score=2.5
77 this is a little house  score=2.5
78 little an it is house  score=2.3
79 this is house small a  score=2.6999999999999997
80 little house a it is  score=1.9
81 it is house small a  score=2.0999999999999996
82 a house little this is  score=2.5
83 house it is a small  score=2.1
84 house little an this is  score=2.8999999999999995
85 this is little an house  score=2.9000000000000004
86 little it is a house  score=1.9000000000000001
87 little house it is an  score=2.3
88 small house an this is  score=3.0999999999999996
89 house it is little an  score=2.3
90 little this is an house  score=2.9000000000000004
91 house a this is small  score=2.7
92 this is house an small  score=3.1
93 a this is house little  score=2.5
94 it is an little house  score=2.3
95 it is house a little  score=1.9
96 it is a house little  score=1.9
97 an small house this is  score=3.0999999999999996
98 an this is small house  score=3.1
99 house an this is small  score=3.1
100 a house it is little  score=1.9
101 an small this is house  score=3.0999999999999996
102 an house it is little  score=2.3
103 an little house it is  score=2.3000000000000003
104 it is house little an  score=2.3
105 small a house this is  score=2.7
106 a small house it is  score=2.1
107 house this is little a  score=2.5
108 a little this is house  score=2.5
109 this is an house small  score=3.1
110 house a little this is  score=2.5
111 house it is an little  score=2.3
112 an house small it is  score=2.5
113 small a it is house  score=2.0999999999999996
114 little this is a house  score=2.5
115 it is house little a  score=1.9000000000000001
116 a small house this is  score=2.7
117 it is small a house  score=2.1
118 an it is small house  score=2.5
119 small house a this is  score=2.7
120 an house small this is  score=3.0999999999999996
121 a house this is little  score=2.5
122 house an little this is  score=2.9000000000000004
123 house it is small a  score=2.0999999999999996
124 house small an this is  score=3.0999999999999996
125 this is house little an  score=2.9000000000000004
126 it is a small house  score=2.1
127 this is small house an  score=3.0999999999999996
128 an house it is small  score=2.5
129 small house it is a  score=2.1
130 house small an it is  score=2.5
131 this is small house a  score=2.6999999999999997
132 house this is a small  score=2.7
133 house it is small an  score=2.5
134 little house an it is  score=2.3
135 house an it is small  score=2.5
136 house a it is small  score=2.1
137 little an house this is  score=2.9000000000000004
138 house this is small a  score=2.6999999999999997
139 small this is an house  score=3.0999999999999996
140 a this is small house  score=2.7
141 an house little it is  score=2.3000000000000003
142 little house this is an  score=2.9000000000000004
143 this is little house a  score=2.5
144 little this is house an  score=2.9000000000000004
145 small house this is a  score=2.7
146 little it is house a  score=1.9000000000000001
147 an it is house little  score=2.3
148 small house a it is  score=2.1
149 house this is little an  score=2.9000000000000004
150 this is small an house  score=3.0999999999999996
151 this is an little house  score=2.9
152 house little an it is  score=2.3
153 house small it is an  score=2.5
154 this is an house little  score=2.9
155 an house this is small  score=3.1
156 house this is an little  score=2.9
157 a house small it is  score=2.1
158 house this is small an  score=3.0999999999999996
159 house small it is a  score=2.1
160 house little a this is  score=2.5
161 an this is little house  score=2.9
162 little house it is a  score=1.9
163 an small it is house  score=2.5
164 a it is house little  score=1.9
165 a house small this is  score=2.7
166 little house an this is  score=2.8999999999999995
167 house small a it is  score=2.1
168 it is house an little  score=2.3
169 an house little this is  score=2.9000000000000004
170 small an house this is  score=3.0999999999999996
171 little this is house a  score=2.5
172 little a it is house  score=1.9
173 it is little house an  score=2.3
174 an little house this is  score=2.9000000000000004
175 small it is house a  score=2.1
176 house it is a little  score=1.9
177 house little a it is  score=1.9
178 this is house an little  score=2.9
179 this is house little a  score=2.5
180 it is little house a  score=1.9000000000000001
181 it is small house an  score=2.5
182 a this is little house  score=2.5
183 little a this is house  score=2.5
184 an this is house little  score=2.9
185 this is a house little  score=2.5
186 house a small it is  score=2.1
187 an house this is little  score=2.9
188 house little this is an  score=2.9000000000000004
189 it is house small an  score=2.5
190 small an this is house  score=3.0999999999999996
191 small this is house a  score=2.6999999999999997
\end{lstlisting}

\clearpage
\begin{thebibliography}{32}

\bibitem{koehn2004}
Koehn, Philipp. ``Pharaoh: a beam search decoder for phrase-based statistical
machine translation models.'' \emph{Machine translation: From real users to
research.} Springer Berlin Heidelberg, 2004. 115-124.

\bibitem{koehn2009}
Koehn, Philipp. ``Statistical Machine Translation (1st ed.)''. \emph{Cambridge
University Press}, 2010.

\bibitem{och2002}
Zens, Richard, Franz Josef Och, and Hermann Ney. ``Phrase-based statistical
machine translation.'' \emph{KI 2002: Advances in Artificial Intelligence.}
Springer Berlin Heidelberg, 2002. 18-32.

\bibitem{callisonburch2005}
Callison-Burch, Chris, Colin Bannard, and Josh Schroeder. ``Scaling phrase-based
statistical machine translation to larger corpora and longer phrases.''
\emph{Proceedings of the 43rd Annual Meeting on Association for Computational
Linguistics.} Association for Computational Linguistics, 2005.

\bibitem{germann2009}
Germann, Ulrich, Eric Joanis, and Samuel Larkin. ``Tightly packed tries: How to
fit large models into memory, and make them load fast, too.'' \emph{Proceedings
of the Workshop on Software Engineering, Testing, and Quality Assurance for
Natural Language Processing.} Association for Computational Linguistics, 2009.

\bibitem{junczysdowmunt2012}
Junczys-Dowmunt, Marcin. ``Phrasal rank-encoding: exploiting phrase redundancy
and translational relations for phrase table compression.'' \emph{The Prague
Bulletin of Mathematical Linguistics} 98.1 (2012): 63-74.

\bibitem{johnson2007}
Johnson, John Howard, et al. ``Improving translation quality by discarding most
of the phrasetable.'' (2007). In \emph{Proceedings of the Joint Conference on
Empirical Methods in Natural Language Processing and Computational Natural
Language Learning}, 2007. 967–975.

\bibitem{moses}
Koehn, Philipp, et al. ``Moses: Open source toolkit for statistical machine
translation.'' \emph{Proceedings of the 45th Annual Meeting of the ACL on
Interactive Poster and Demonstration Sessions.} Association for Computational
Linguistics, 2007.

\bibitem{tomeh2009}
Tomeh, Nadi, Nicola Cancedda, and Marc Dymetman. ``Complexity-based phrase-table
filtering for statistical machine translation.'' \emph{MTSummit XII} (2009).

\bibitem{eck2007}
Eck, Stephen Vogal Matthias, and Alex Waibel. ``Estimating phrase pair relevance
for translation model pruning.'' \emph{MTSummit XI} (2007).

\bibitem{wang2012}
Ling, Wang, et al. ``Entropy-based pruning for phrase-based machine
translation.'' \emph{Proceedings of the 2012 Joint Conference on Empirical
Methods in Natural Language Processing and Computational Natural Language
Learning.} Association for Computational Linguistics, 2012.

\bibitem{quirk2007}
Quirk, Chris, and R. Moore. ``Faster beam-search decoding for phrasal
statistical machine translation.'' \emph{Machine Translation Summit XI} (2007).

\bibitem{wuebker2012}
Wuebker, Joern, Hermann Ney, and Richard Zens. ``Fast and scalable decoding with
language model look-ahead for phrase-based statistical machine translation.''
\emph{Proceedings of the 50th Annual Meeting of the Association for
Computational Linguistics: Short Papers-Volume 2.} Association for Computational
Linguistics, 2012.

\bibitem{och2001}
Och, Franz Josef, Nicola Ueffing, and Hermann Ney. ``An efficient A* search
algorithm for statistical machine translation.'' \emph{Proceedings of the
workshop on Data-driven methods in machine translation-Volume 14.} Association
for Computational Linguistics, 2001.

\bibitem{tillman2006}
Tillmann, Christoph. ``Efficient dynamic programming search algorithms for
phrase-based SMT.'' \emph{Proceedings of the Workshop on Computationally Hard
Problems and Joint Inference in Speech and Language Processing.} Association for
Computational Linguistics, 2006.

\bibitem{chang2011}
Chang, Yin-Wen, and Michael Collins. ``Exact decoding of phrase-based
translation models through lagrangian relaxation.'' \emph{Proceedings of the
Conference on Empirical Methods in Natural Language Processing.} Association for
Computational Linguistics, 2011.

\bibitem{zaslavskiy2009}
Zaslavskiy, Mikhail, Marc Dymetman, and Nicola Cancedda. ``Phrase-based
statistical machine translation as a travelling salesman problem.''
\emph{Proceedings of the Joint Conference of the 47th Annual Meeting of the ACL
and the 4th International Joint Conference on Natural Language Processing of the
AFNLP: Volume 1-Volume 1.} Association for Computational Linguistics, 2009.

\end{thebibliography}

\end{document}
