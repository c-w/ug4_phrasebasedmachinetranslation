# Phrase-based decoding

This package implements a stack-based decoder for phrase-based statistical
machine translation.

## How to run

First, ensure that the project's binary file is on the Java class-path.  For
example, run `export CLASSPATH=dist/mt2.jar:$CLASSPATH` or use `java -cp
dist/mt2.jar`.

The implementation can be run using the following commands.

Read a phrase table stored on disk at the path *$pt* and find all the applicable
spans in the sentences in the file *$ss*:

`java mt2.phrasetable.PhraseTable $pt $ss`

Translate the source sentences stored at the path *$ss* using the phrase table
at the path *$pt*, using the configuration file at *$cfg*:

`java mt2.decoder.Decoder $cfg $pt $ss`

Sample configuration files reside in the *config* directory.

Alternatively, the project's Apache Ant build file *build.xml* can be used to
produce the outputs defined in the assignment handout:

- Produce the outputs of Section 1.1: `ant run-phrase-table`
- Produce the outputs of Section 1.2: `ant run-decoder-no-reordering`
- Produce the outputs of Section 1.3: `ant run-decoder-unlimited-reordering`

The project can be compiled and packaged with `ant jar`.

## Configuration options

The decoder configuration file should be in the following format:

> option1=value1
>
> option2=value2
>
> ...

> optionN=valueN

The decoder accepts the following configuration options:

```
#!cfg
##############################################################
########### set the decoder's re-ordering strategy ###########
##############################################################

# grow the translation left-to-right in one continuous block
# only the next word can be translated
decoder.class=MonotonicDecoder

# allow any word in the source sentence to be translated next (slow)
decoder.class=UnlimitedReorderingDecoder

# allow words within a +-N word window to be translated next
# N should be a positive integer (N=0,1,2,...)
decoder.class=LimitedReorderingDecoder
decoder.neighborhood=N

##############################################################
########## set the decoder's stack pruning strategy ##########
##############################################################

# do not perform any stack pruning (very slow!)
decoder.stack=UnrestrictedStack

# limit the size of the stack to at most N hypotheses
# discard the worst hypothesis whenever the stack would grow past N entries
# N should be a non-zero positive integer (N=1,2,3,...)
decoder.stack=HistogramPruningStack
decoder.stack.maxSize=N

# discard all hypotheses that have a score worse than K times the score of the
# best hypothesis in the stack
# K should be a real in (0,1]
decoder.stack=ThresholdPruningStack
decoder.stack.threshold=K

##############################################################
####### set the decoder's phrase-table pruning strategy ######
##############################################################

# limit the maximum number of translation options per entry in the phrase-table
# leads to dramatic speed-ups with little to no loss in translation quality
phraseTable.maxTranslationOptions=1,2,...
```
